#!/bin/bash -x
#     ROCKY 8 HARDENING SCRIPT
#
##  THIS SCRIPT IS NOT IDEMPOTENT (YOU CANNOT RUN IT TWICE)
## CONFIRM YOU HAVE A SNAPSHOT BEFORE EXECUTE IT.
#
#  This script requires that /home has its own partition,
# the existance of a "rl" volume group with 4GiB of
# available space.
#
##  THIS SCRIPT IS NOT IDEMPOTENT (YOU CANNOT RUN IT TWICE)
## CONFIRM YOU HAVE A SNAPSHOT BEFORE EXECUTE IT.

# Validate there is enough space in VGS

echo -e "\nTHIS SCRIPT IS NOT IDEMPOTENT (YOU CANNOT RUN IT TWICE)\n"
echo -e "CONFIRM YOU HAVE A SNAPSHOT BEFORE EXECUTE IT\n."
read -p "The volume grop _rl_ (VGS) has enough space (3.99 GiB)? (yes/no) " yn

case $yn in
	yes ) echo ok, we will proceed;;
	no ) echo exiting...;
		exit;;
	* ) echo invalid response. Just yes or no;
		exit 1;;
esac


#  This section implements hardening for the filesystem by
# creating the own mount points for `/var/log`, `/var/log/audit`,
# configures `/home` mounting options, changes `/tmp` mount point,
# add `shm` mount options.
echo "Changing filesystem"

# Backup dirs
mkdir -p /tmp/var/log_bkp
mkdir -p /tmp/var/log/audit_bkp
mkdir -p /tmp_bkp

# Modify fstab
cp /etc/fstab /etc/fstab.bkp
sed -i '/[/]home/ s/^/#/' /etc/fstab
cat << EOF >> /etc/fstab
# Hardening. Step 6a
/dev/mapper/rl-varloglv /var/log            xfs     defaults        0 0
# Hardening. Step 6b
/dev/rl/varlogauditlv   /var/log/audit      xfs     defaults        0 0
# Hardening. Step 7
/tmp                     /var/tmp                none   rw,noexec,nosuid,nodev,bind 0 0
# Hardening. Step 8
/dev/mapper/rl-home   /home                 xfs     defaults,nodev  0 0
# Hardening. Step 9
tmpfs                     /dev/shm              tmpfs    defaults,noexec,nodev,nosuid,seclabel 0 0
EOF

# Create and mount filesystem
lvcreate -L 1.9G -n varloglv rl
mkfs.xfs -f /dev/mapper/rl-varloglv
mv /var/log/* /tmp/var/log_bkp/
mount /var/log
cp -R /tmp/var/log_bkp/* /var/log/
restorecon -FvR /var/log
lvcreate -L 1.9G -n varlogauditlv rl
mkfs.xfs -f /dev/rl/varlogauditlv
mv /var/log/audit/* /tmp/var/log/audit_bkp/
mount /var/log/audit
cp -R /tmp/var/log/audit_bkp/* /var/log/audit/
restorecon -FvR /var/log/audit
chcon -t auditd_log_t /var/log/audit/audit.log
chcon -t auditd_log_t /var/log/audit
service auditd restart
mv /tmp/* /tmp_bkp/
mount /var/tmp
cp -R /tmp_bkp/* /tmp/
echo "Completed Disks Hardening"

## Check sticky bit is enabled for all world-writable directories
## This section should return empty
df --local -P | awk {'if (NR!=1) print $6'} | xargs -I '{}' find '{}' -xdev -type d \( -perm -0002 -a ! -perm -1000 \) 2>/dev/null

## GPG Enable Repositories
rpm -q gpg-pubkey --qf '%{name}-%{version}-%{release} --> %{summary}\n'
cp /etc/yum.conf /etc/yum.conf.bkp
sed -i 's/gpgcheck=[0-9]/gpgcheck=1/g' /etc/yum.conf

## OS packages configurations
insecure_software=( "omi" "telnet" "rsh" "rlogin" "ypserv" "ypbind" "tftp" "talk" "chargen" "daytime" "echo" "tcpmux")
required_sofware=( "chrony" "oddjob" )
for insecure_package in "${insecure_software[@]}"
do
    # Should be just list?
    dnf -y remove "$insecure_package"
done
for package in "${required_sofware[@]}"
do
    dnf -y install "$package"
done
yum check-update >> /root/yum_check-update_$(date +'%Y%m%d_%H-%M-%S').log
yum -y update

## Protecting Grub
echo -e "\n Get ready for grub password\n"
grub2-setpassword
chown root:root /boot/grub2/grub.cfg
chmod og-rwx /boot/grub2/grub.cfg

## Services
unconfigured_services=( "kdump" "vsftpd" "bind" "dhcpd" "smb" "nmb" "httpd" "cockpit" )
for unconfigured_service in "${unconfigured_services[@]}"
do
    systemctl disable --now "$unconfigured_service"
done
required_services=( "sssd" "oddjobd" )
for required_service in "${required_services[@]}"
do
    systemctl enable --now "$required_service"
done

## Kernel variable should return 2
sysctl kernel.randomize_va_space

## Umask Value
#umask_value="$(grep "umask [0-9][0-9][0-9]" /etc/init.d/functions)"
umask_value="$(umask)"
if [ "$umask_value" == "0022" ] || [ "$umask_value" == "0027" ]; then
    echo umask OK
else
    echo Critical error, recover snapshot, and set umask 022 before execute script
    exit 1
fi

## Sysctl configs
#!/bin/bash -x
cat << EOF >> /etc/sysctl.d/99-sysctl.conf
net.ipv4.ip_forward = 0
net.ipv4.conf.all.forwarding = 0
net.ipv4.conf.all.accept_redirects = 0
net.ipv6.conf.all.accept_redirects = 0
net.ipv4.icmp_echo_ignore_broadcasts = 1
net.ipv4.icmp_ignore_bogus_error_responses = 1
net.ipv4.tcp_syncookies = 1
EOF
sysctl -p

## Create user to connect via ssh
useradd localroot
usermod localroot -aG wheel
echo "Insert password for new administrative user localroot"
passwd localroot

## Setup SSHD
cp /etc/ssh/sshd_config /etc/ssh/sshd_config.bkp
sed -i '/PermitRootLogin/ s/^/#/' /etc/ssh/sshd_config
cat << EOF >> /etc/ssh/sshd_config
# Hardening Configuration
LogLevel INFO
PermitRootLogin no
PermitEmptyPasswords no
EOF
systemctl restart sshd

## Force SELinux to Enforced
cp /etc/selinux/config /etc/selinux/config.bkp
sed -i s/^SELINUX=.*$/SELINUX=enforcing/ /etc/selinux/config

## Configure NTP
timedatectl set-timezone Pacific/Auckland
mv /etc/localtime /etc/localtime.back
ln -s /usr/share/zoneinfo/Pacific/Auckland /etc/localtime
systemctl enable --now chronyd
timedatectl set-ntp true

## Pam https://access.redhat.com/solutions/5027331
## Passwd algorithm 512 by default
## grep 512 /etc/libuser.conf /etc/login.defs
path_system_auth="/etc/authselect/custom/password-policy/system-auth"
path_password_auth="/etc/authselect/custom/password-policy/password-auth"
authselect apply-changes -b --backup=sssd.backup
authselect create-profile password-policy -b sssd
authselect select custom/password-policy --force
authselect current
authselect enable-feature with-mkhomedir
authselect enable-feature with-faillock
cp $path_system_auth /etc/authselect/custom/password-policy/system-auth.bkp
cp $path_password_auth /etc/authselect/custom/password-policy/password-auth.bkp
sed -i '/pam_pwquality.so/a password requisite pam_pwhistory.so remember=5 use_authtok' $path_system_auth
sed -i '/pam_pwquality.so/a password requisite pam_pwhistory.so remember=5 use_authtok' $path_password_auth
sed -i '/pam_pwquality.so/ s/$/ enforce_for_root/' $path_system_auth
sed -i '/pam_pwquality.so/ s/$/ enforce_for_root/' $path_password_auth
cat << EOF >> /etc/security/pwquality.conf
difok = 5
minlen = 16
dcredit = -1
ucredit = 0
lcredit = -1
ocredit = -1
minclass = 4
maxrepeat = 0
maxclassrepeat = 0
gecoscheck = 1
EOF
authselect apply-changes

# Secure tty https://access.redhat.com/solutions/6758921
path_pam_login="/etc/pam.d/login"
path_pam_remote="/etc/pam.d/remote"
cp $path_pam_login /etc/pam.d/login.bkp
cp $path_pam_remote /etc/pam.d/remote.bkp
sed -i '/PAM-1.0/a auth [user_unknown=ignore success=ok ignore=ignore default=bad] pam_securetty.so' $path_pam_login
sed -i '/PAM-1.0/a auth required pam_securetty.so' $path_pam_remote
cat << EOF >> /etc/securetty
tty1
tty2
tty3
EOF
chown root:root /etc/securetty
chmod 400 /etc/securetty

## Firewall
firewall-cmd --zone=public --add-port=1270/tcp --permanent
firewall-cmd --reload

echo "Hardening Completed"
